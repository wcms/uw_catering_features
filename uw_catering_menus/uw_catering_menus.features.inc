<?php

/**
 * @file
 * uw_catering_menus.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function uw_catering_menus_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_image_default_styles().
 */
function uw_catering_menus_image_default_styles() {
  $styles = array();

  // Exported image style: catering_gallery_thumbnail.
  $styles['catering_gallery_thumbnail'] = array(
    'label' => 'Catering Gallery Thumbnail',
    'effects' => array(
      6 => array(
        'name' => 'focal_point_scale_and_crop',
        'data' => array(
          'width' => 385,
          'height' => 320,
          'focal_point_advanced' => array(
            'shift_x' => '',
            'shift_y' => '',
          ),
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: gallery_lightbox.
  $styles['gallery_lightbox'] = array(
    'label' => 'Gallery lightbox',
    'effects' => array(
      7 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => 1200,
          'height' => '',
          'upscale' => 0,
        ),
        'weight' => 1,
      ),
    ),
  );

  return $styles;
}

/**
 * Implements hook_node_info().
 */
function uw_catering_menus_node_info() {
  $items = array(
    'menu' => array(
      'name' => t('Menu'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'menu_group' => array(
      'name' => t('Menu Group'),
      'base' => 'node_content',
      'description' => t('A group of associated menus, with a navigation menu.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'menu_type_landing_page' => array(
      'name' => t('Menu Type Landing page'),
      'base' => 'node_content',
      'description' => t('Landing page for a menu type (e.g. Galas & Weddings, Corporate, Social etc.)'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}

/**
 * Implements hook_paragraphs_info().
 */
function uw_catering_menus_paragraphs_info() {
  $items = array(
    'menu_item' => array(
      'name' => 'Menu item',
      'bundle' => 'menu_item',
      'locked' => '1',
    ),
    'menu_item_price' => array(
      'name' => 'Menu item with price',
      'bundle' => 'menu_item_price',
      'locked' => '1',
    ),
    'menu_option' => array(
      'name' => 'Menu option',
      'bundle' => 'menu_option',
      'locked' => '1',
    ),
    'menu_section' => array(
      'name' => 'Menu section',
      'bundle' => 'menu_section',
      'locked' => '1',
    ),
  );
  return $items;
}
