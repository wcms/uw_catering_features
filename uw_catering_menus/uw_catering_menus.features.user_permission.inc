<?php

/**
 * @file
 * uw_catering_menus.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function uw_catering_menus_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create menu content'.
  $permissions['create menu content'] = array(
    'name' => 'create menu content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'create menu_group content'.
  $permissions['create menu_group content'] = array(
    'name' => 'create menu_group content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'create menu_type_landing_page content'.
  $permissions['create menu_type_landing_page content'] = array(
    'name' => 'create menu_type_landing_page content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any menu content'.
  $permissions['delete any menu content'] = array(
    'name' => 'delete any menu content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any menu_group content'.
  $permissions['delete any menu_group content'] = array(
    'name' => 'delete any menu_group content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any menu_type_landing_page content'.
  $permissions['delete any menu_type_landing_page content'] = array(
    'name' => 'delete any menu_type_landing_page content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own menu content'.
  $permissions['delete own menu content'] = array(
    'name' => 'delete own menu content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own menu_group content'.
  $permissions['delete own menu_group content'] = array(
    'name' => 'delete own menu_group content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own menu_type_landing_page content'.
  $permissions['delete own menu_type_landing_page content'] = array(
    'name' => 'delete own menu_type_landing_page content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any menu content'.
  $permissions['edit any menu content'] = array(
    'name' => 'edit any menu content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any menu_group content'.
  $permissions['edit any menu_group content'] = array(
    'name' => 'edit any menu_group content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any menu_type_landing_page content'.
  $permissions['edit any menu_type_landing_page content'] = array(
    'name' => 'edit any menu_type_landing_page content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own menu content'.
  $permissions['edit own menu content'] = array(
    'name' => 'edit own menu content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own menu_group content'.
  $permissions['edit own menu_group content'] = array(
    'name' => 'edit own menu_group content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own menu_type_landing_page content'.
  $permissions['edit own menu_type_landing_page content'] = array(
    'name' => 'edit own menu_type_landing_page content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  return $permissions;
}
