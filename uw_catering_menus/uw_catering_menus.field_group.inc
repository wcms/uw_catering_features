<?php

/**
 * @file
 * uw_catering_menus.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function uw_catering_menus_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_dietary_icons|paragraphs_item|menu_item|form';
  $field_group->group_name = 'group_dietary_icons';
  $field_group->entity_type = 'paragraphs_item';
  $field_group->bundle = 'menu_item';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Dietary icons',
    'weight' => '2',
    'children' => array(
      0 => 'field_menu_item_dietary',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Dietary icons',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-dietary-icons field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $field_groups['group_dietary_icons|paragraphs_item|menu_item|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_label_price|paragraphs_item|menu_option|form';
  $field_group->group_name = 'group_label_price';
  $field_group->entity_type = 'paragraphs_item';
  $field_group->bundle = 'menu_option';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Label + Price',
    'weight' => '0',
    'children' => array(
      0 => 'field_menu_option_label',
      1 => 'field_menu_price',
    ),
    'format_type' => 'html-element',
    'format_settings' => array(
      'label' => 'Label + Price',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-label-price field-group-html-element fields-inline',
        'element' => 'div',
        'show_label' => '0',
        'label_element' => 'div',
        'attributes' => '',
      ),
    ),
  );
  $field_groups['group_label_price|paragraphs_item|menu_option|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_menu_links|node|menu_type_landing_page|default';
  $field_group->group_name = 'group_menu_links';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'menu_type_landing_page';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'View our Menus',
    'weight' => '1',
    'children' => array(
      0 => 'field_menu_links',
      1 => 'field_menu_image',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'View our Menus',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'menu-links-wrapper fixwidth',
        'description' => '',
        'show_label' => '1',
        'label_element' => 'h2',
        'effect' => 'none',
        'speed' => 'fast',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_menu_links|node|menu_type_landing_page|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_menu_options|paragraphs_item|menu_item_price|form';
  $field_group->group_name = 'group_menu_options';
  $field_group->entity_type = 'paragraphs_item';
  $field_group->bundle = 'menu_item_price';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Options & dietary icons',
    'weight' => '3',
    'children' => array(
      0 => 'field_menu_options',
      1 => 'field_menu_item_dietary',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Options & dietary icons',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-menu-options field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $field_groups['group_menu_options|paragraphs_item|menu_item_price|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_menu_price|paragraphs_item|menu_item_price|form';
  $field_group->group_name = 'group_menu_price';
  $field_group->entity_type = 'paragraphs_item';
  $field_group->bundle = 'menu_item_price';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Price',
    'weight' => '0',
    'children' => array(
      0 => 'field_menu_price',
      1 => 'field_price_type',
    ),
    'format_type' => 'html-element',
    'format_settings' => array(
      'label' => 'Price',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-menu-price field-group-html-element fields-inline',
        'element' => 'div',
        'show_label' => '0',
        'label_element' => 'div',
        'attributes' => '',
      ),
    ),
  );
  $field_groups['group_menu_price|paragraphs_item|menu_item_price|form'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('Dietary icons');
  t('Label + Price');
  t('Options & dietary icons');
  t('Price');
  t('View our Menus');

  return $field_groups;
}
