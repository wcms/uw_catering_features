<?php

/**
 * @file
 * uw_catering_views.features.inc
 */

/**
 * Implements hook_views_api().
 */
function uw_catering_views_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
