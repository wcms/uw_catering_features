<?php

/**
 * @file
 * uw_catering_base.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function uw_catering_base_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'field_content_items'.
  $field_bases['field_content_items'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_content_items',
    'field_permissions' => array(
      'type' => 0,
    ),
    'indexes' => array(
      'target_id' => array(
        0 => 'target_id',
      ),
    ),
    'locked' => 0,
    'module' => 'entityreference',
    'settings' => array(
      'entity_translation_sync' => FALSE,
      'handler' => 'base',
      'handler_settings' => array(
        'behaviors' => array(
          'views-select-list' => array(
            'status' => 0,
          ),
        ),
        'sort' => array(
          'type' => 'none',
        ),
        'target_bundles' => array(
          'contact' => 'contact',
          'menu' => 'menu',
          'menu_group' => 'menu_group',
        ),
      ),
      'target_type' => 'node',
    ),
    'translatable' => 0,
    'type' => 'entityreference',
  );

  // Exported field_base: 'field_gallery_image'.
  $field_bases['field_gallery_image'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_gallery_image',
    'field_permissions' => array(
      'type' => 0,
    ),
    'indexes' => array(
      'fid' => array(
        0 => 'fid',
      ),
    ),
    'locked' => 0,
    'module' => 'image',
    'settings' => array(
      'default_image' => 0,
      'entity_translation_sync' => array(
        0 => 'fid',
      ),
      'uri_scheme' => 'public',
    ),
    'translatable' => 0,
    'type' => 'image',
  );

  // Exported field_base: 'field_image_position'.
  $field_bases['field_image_position'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_image_position',
    'field_permissions' => array(
      'type' => 0,
    ),
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(
        'left' => 'Left',
        'right' => 'Right',
      ),
      'allowed_values_function' => '',
      'entity_translation_sync' => FALSE,
    ),
    'translatable' => 0,
    'type' => 'list_text',
  );

  // Exported field_base: 'field_section_heading'.
  $field_bases['field_section_heading'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_section_heading',
    'field_permissions' => array(
      'type' => 0,
    ),
    'indexes' => array(
      'format' => array(
        0 => 'format',
      ),
    ),
    'locked' => 0,
    'module' => 'text',
    'settings' => array(
      'entity_translation_sync' => FALSE,
      'max_length' => 255,
    ),
    'translatable' => 0,
    'type' => 'text',
  );

  // Exported field_base: 'field_text'.
  $field_bases['field_text'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_text',
    'field_permissions' => array(
      'type' => 0,
    ),
    'indexes' => array(
      'format' => array(
        0 => 'format',
      ),
    ),
    'locked' => 0,
    'module' => 'text',
    'settings' => array(
      'entity_translation_sync' => FALSE,
    ),
    'translatable' => 0,
    'type' => 'text_long',
  );

  // Exported field_base: 'field_view_embed'.
  $field_bases['field_view_embed'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_view_embed',
    'field_permissions' => array(
      'type' => 0,
    ),
    'indexes' => array(
      'view_id' => array(
        0 => 'view_id',
      ),
    ),
    'locked' => 0,
    'module' => 'viewreference',
    'settings' => array(
      'append_id' => 0,
      'arguments' => array(
        'delimiter' => '/',
        'dsv_arguments' => 0,
        'label' => '!field_label arguments',
        'php_arguments' => 0,
        'rows' => 1,
      ),
      'entity_translation_sync' => FALSE,
      'referenceable_tags' => array(
        'allow' => '',
        'deny' => '',
      ),
      'referenceable_views' => array(
        'admin_views_comment:default' => 0,
        'admin_views_comment:system_1' => 0,
        'admin_views_comment:system_2' => 0,
        'admin_views_node:default' => 0,
        'admin_views_node:system_1' => 0,
        'admin_views_taxonomy_term:default' => 0,
        'admin_views_taxonomy_term:system_1' => 0,
        'admin_views_user:default' => 0,
        'admin_views_user:system_1' => 0,
        'all_menus:block_1' => 'all_menus:block_1',
        'all_menus:default' => 0,
        'archive:block' => 0,
        'archive:default' => 0,
        'archive:page' => 0,
        'backlinks:block' => 0,
        'backlinks:default' => 0,
        'backlinks:page' => 0,
        'comments_recent:block' => 0,
        'comments_recent:default' => 0,
        'comments_recent:page' => 0,
        'feeds_log:default' => 0,
        'feeds_log:page_1' => 0,
        'feeds_log:page_2' => 0,
        'feeds_log:page_3' => 0,
        'forward_clickthroughs:block' => 0,
        'forward_clickthroughs:default' => 0,
        'forward_forwards:block' => 0,
        'forward_forwards:default' => 0,
        'forward_recent:block' => 0,
        'forward_recent:default' => 0,
        'frontpage:default' => 0,
        'frontpage:feed' => 0,
        'frontpage:page' => 0,
        'glossary:attachment' => 0,
        'glossary:default' => 0,
        'glossary:page' => 0,
        'list_node_page_titles:default' => 0,
        'list_node_page_titles:page_1' => 0,
        'manage_web_pages:default' => 0,
        'manage_web_pages:manage_web_pages' => 0,
        'page_banner:block' => 0,
        'page_banner:default' => 0,
        'people:block' => 0,
        'people:default' => 0,
        'people:page' => 0,
        'redirects:default' => 0,
        'redirects:page_admin' => 0,
        'redirects:page_user' => 0,
        'taxonomy_term:default' => 0,
        'taxonomy_term:feed' => 0,
        'taxonomy_term:page' => 0,
        'tracker:default' => 0,
        'tracker:page' => 0,
        'uw_catering_people:block' => 'uw_catering_people:block',
        'uw_catering_people:default' => 0,
        'uw_catering_people:page' => 0,
        'uw_people_profile_pages:attachment_1' => 0,
        'uw_people_profile_pages:block_1' => 0,
        'uw_people_profile_pages:default' => 0,
        'uw_people_profile_pages:manage_people_profiles' => 0,
        'uw_people_profile_pages:page_1' => 0,
        'uw_people_profile_pages:page_2' => 0,
        'uw_people_profile_pages:rearrange_people_profiles' => 0,
        'uw_staff_contacts_vw:default' => 0,
        'uw_staff_contacts_vw:page' => 0,
        'uw_staff_contacts_vw:page_1' => 0,
        'uw_staff_contacts_vw:page_2' => 0,
        'uw_staff_contacts_vw:rearrange_staff_contacts' => 0,
        'uw_workbench_moderation:default' => 0,
        'uw_workbench_moderation:drafts_page' => 0,
        'uw_workbench_moderation:needs_review_page' => 0,
        'uw_workbench_recent_content:block_1' => 0,
        'uw_workbench_recent_content:default' => 0,
        'uw_workbench_recent_content:page_1' => 0,
        'web_resources_news:default' => 0,
        'web_resources_news:web_resources_news_feed_block' => 0,
        'webform_analysis:default' => 0,
        'webform_results:default' => 0,
        'webform_submissions:default' => 0,
        'webform_webforms:default' => 0,
        'workbench_current_user:block_1' => 0,
        'workbench_current_user:default' => 0,
        'workbench_edited:block_1' => 0,
        'workbench_edited:default' => 0,
        'workbench_edited:page_1' => 0,
        'workbench_files_list:default' => 0,
        'workbench_files_list:page_1' => 0,
        'workbench_files_list:page_2' => 0,
        'workbench_moderation:default' => 0,
        'workbench_moderation:drafts_page' => 0,
        'workbench_moderation:needs_review_page' => 0,
        'workbench_moderation_archive:archive_page_view' => 0,
        'workbench_moderation_archive:default' => 0,
        'workbench_recent_content:block_1' => 0,
        'workbench_recent_content:default' => 0,
        'workbench_recent_content:page_1' => 0,
      ),
      'skip_default' => 1,
      'skip_disabled' => 0,
      'skip_empty' => 0,
      'sort_views' => 1,
    ),
    'translatable' => 0,
    'type' => 'viewreference',
  );

  return $field_bases;
}
