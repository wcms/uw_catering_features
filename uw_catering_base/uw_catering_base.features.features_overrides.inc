<?php

/**
 * @file
 * uw_catering_base.features.features_overrides.inc
 */

/**
 * Implements hook_features_override_default_overrides().
 */
function uw_catering_base_features_override_default_overrides() {
  // This code is only used for UI in features. Exported alters hooks do the magic.
  $overrides = array();

  // Exported overrides for: variable
  $overrides["variable.pathauto_node_pattern.value"] = '[node:title]';

 return $overrides;
}
