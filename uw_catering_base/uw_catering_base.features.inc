<?php

/**
 * @file
 * uw_catering_base.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function uw_catering_base_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "linkit" && $api == "linkit_profiles") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function uw_catering_base_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_strongarm_alter().
 */
function uw_catering_base_strongarm_alter(&$data) {
  if (isset($data['pathauto_node_pattern'])) {
    $data['pathauto_node_pattern']->value = '[node:title]'; /* WAS: 'content/[node:title]' */
  }
}

/**
 * Implements hook_image_default_styles().
 */
function uw_catering_base_image_default_styles() {
  $styles = array();

  // Exported image style: catering_banner__med.
  $styles['catering_banner__med'] = array(
    'label' => 'Catering banner (med)',
    'effects' => array(
      14 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 1000,
          'height' => 494,
        ),
        'weight' => 2,
      ),
    ),
  );

  // Exported image style: catering_banner__sm.
  $styles['catering_banner__sm'] = array(
    'label' => 'Catering banner (sm)',
    'effects' => array(
      13 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 600,
          'height' => 256,
        ),
        'weight' => 2,
      ),
    ),
  );

  // Exported image style: catering_banner__wide.
  $styles['catering_banner__wide'] = array(
    'label' => 'Catering banner (wide)',
    'effects' => array(
      10 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 1500,
          'height' => 740,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: catering_short_banner__med.
  $styles['catering_short_banner__med'] = array(
    'label' => 'Catering short banner (med)',
    'effects' => array(
      20 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 1000,
          'height' => 426,
        ),
        'weight' => 2,
      ),
    ),
  );

  // Exported image style: catering_short_banner__sm.
  $styles['catering_short_banner__sm'] = array(
    'label' => 'Catering short banner (sm)',
    'effects' => array(
      19 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 400,
          'height' => 250,
        ),
        'weight' => 2,
      ),
    ),
  );

  // Exported image style: catering_short_banner__wide.
  $styles['catering_short_banner__wide'] = array(
    'label' => 'Catering short banner (wide)',
    'effects' => array(
      18 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 1500,
          'height' => 640,
        ),
        'weight' => 2,
      ),
    ),
  );

  return $styles;
}

/**
 * Implements hook_node_info().
 */
function uw_catering_base_node_info() {
  $items = array(
    'basic_page' => array(
      'name' => t('Basic Page'),
      'base' => 'node_content',
      'description' => t('Use this for general content pages such as About Us or Our Venue.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}

/**
 * Implements hook_paragraphs_info().
 */
function uw_catering_base_paragraphs_info() {
  $items = array(
    'banner_with_link' => array(
      'name' => 'Banner image with link',
      'bundle' => 'banner_with_link',
      'locked' => '1',
    ),
    'content_section' => array(
      'name' => 'Content section',
      'bundle' => 'content_section',
      'locked' => '1',
    ),
    'gallery' => array(
      'name' => 'Gallery',
      'bundle' => 'gallery',
      'locked' => '1',
    ),
    'text' => array(
      'name' => 'Text',
      'bundle' => 'text',
      'locked' => '1',
    ),
    'text_with_image' => array(
      'name' => 'Text with image',
      'bundle' => 'text_with_image',
      'locked' => '1',
    ),
    'views_embed' => array(
      'name' => 'Views embed',
      'bundle' => 'views_embed',
      'locked' => '1',
    ),
  );
  return $items;
}
