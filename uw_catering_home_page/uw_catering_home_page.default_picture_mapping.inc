<?php

/**
 * @file
 * uw_catering_home_page.default_picture_mapping.inc
 */

/**
 * Implements hook_default_picture_mapping().
 */
function uw_catering_home_page_default_picture_mapping() {
  $export = array();

  $picture_mapping = new PictureMapping();
  $picture_mapping->disabled = FALSE; /* Edit this to true to make a default picture_mapping disabled initially */
  $picture_mapping->api_version = 2;
  $picture_mapping->label = 'Catering homepage banners';
  $picture_mapping->machine_name = 'catering_homepage_banners';
  $picture_mapping->breakpoint_group = 'uw_catering_theme';
  $picture_mapping->mapping = array(
    'breakpoints.theme.uw_catering_theme.mobile' => array(
      '1x' => array(
        'mapping_type' => 'image_style',
        'image_style' => 'catering_homepage_banner_sm',
      ),
    ),
    'breakpoints.theme.uw_catering_theme.narrow' => array(
      '1x' => array(
        'mapping_type' => 'image_style',
        'image_style' => 'catering_homepage_banner_lg',
      ),
    ),
    'breakpoints.theme.uw_catering_theme.wide' => array(
      '1x' => array(
        'mapping_type' => 'image_style',
        'image_style' => 'catering_homepage_banner_lg',
      ),
    ),
  );
  $export['catering_homepage_banners'] = $picture_mapping;

  return $export;
}
