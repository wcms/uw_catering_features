<?php

/**
 * @file
 * uw_catering_home_page.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function uw_catering_home_page_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "breakpoints" && $api == "default_breakpoint_group") {
    return array("version" => "1");
  }
  if ($module == "breakpoints" && $api == "default_breakpoints") {
    return array("version" => "1");
  }
  if ($module == "picture" && $api == "default_picture_mapping") {
    return array("version" => "2");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_image_default_styles().
 */
function uw_catering_home_page_image_default_styles() {
  $styles = array();

  // Exported image style: catering_homepage_banner_lg.
  $styles['catering_homepage_banner_lg'] = array(
    'label' => 'Catering homepage banner (wide)',
    'effects' => array(
      1 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 800,
          'height' => 450,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: catering_homepage_banner_sm.
  $styles['catering_homepage_banner_sm'] = array(
    'label' => 'Catering homepage banner (sm)',
    'effects' => array(
      2 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 600,
          'height' => 350,
        ),
        'weight' => 1,
      ),
    ),
  );

  return $styles;
}

/**
 * Implements hook_node_info().
 */
function uw_catering_home_page_node_info() {
  $items = array(
    'uw_catering_home_page' => array(
      'name' => t('Home page'),
      'base' => 'node_content',
      'description' => t('Home page for uWaterloo Catering Services.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
