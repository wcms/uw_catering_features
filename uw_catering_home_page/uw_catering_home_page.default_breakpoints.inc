<?php

/**
 * @file
 * uw_catering_home_page.default_breakpoints.inc
 */

/**
 * Implements hook_default_breakpoints().
 */
function uw_catering_home_page_default_breakpoints() {
  $export = array();

  $breakpoint = new stdClass();
  $breakpoint->disabled = FALSE; /* Edit this to true to make a default breakpoint disabled initially */
  $breakpoint->api_version = 1;
  $breakpoint->machine_name = 'breakpoints.theme.uw_catering_theme.mobile';
  $breakpoint->name = 'mobile';
  $breakpoint->breakpoint = '(max-width: 600px)';
  $breakpoint->source = 'uw_catering_theme';
  $breakpoint->source_type = 'theme';
  $breakpoint->status = 1;
  $breakpoint->weight = 0;
  $breakpoint->multipliers = array(
    '1x' => '1x',
  );
  $export['breakpoints.theme.uw_catering_theme.mobile'] = $breakpoint;

  $breakpoint = new stdClass();
  $breakpoint->disabled = FALSE; /* Edit this to true to make a default breakpoint disabled initially */
  $breakpoint->api_version = 1;
  $breakpoint->machine_name = 'breakpoints.theme.uw_catering_theme.narrow';
  $breakpoint->name = 'narrow';
  $breakpoint->breakpoint = '(max-width: 960px)';
  $breakpoint->source = 'uw_catering_theme';
  $breakpoint->source_type = 'theme';
  $breakpoint->status = 1;
  $breakpoint->weight = 1;
  $breakpoint->multipliers = array(
    '1x' => '1x',
  );
  $export['breakpoints.theme.uw_catering_theme.narrow'] = $breakpoint;

  $breakpoint = new stdClass();
  $breakpoint->disabled = FALSE; /* Edit this to true to make a default breakpoint disabled initially */
  $breakpoint->api_version = 1;
  $breakpoint->machine_name = 'breakpoints.theme.uw_catering_theme.wide';
  $breakpoint->name = 'wide';
  $breakpoint->breakpoint = '(min-width: 961px)';
  $breakpoint->source = 'uw_catering_theme';
  $breakpoint->source_type = 'theme';
  $breakpoint->status = 1;
  $breakpoint->weight = 2;
  $breakpoint->multipliers = array(
    '1x' => '1x',
  );
  $export['breakpoints.theme.uw_catering_theme.wide'] = $breakpoint;

  return $export;
}
