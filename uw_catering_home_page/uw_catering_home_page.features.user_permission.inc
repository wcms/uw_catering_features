<?php

/**
 * @file
 * uw_catering_home_page.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function uw_catering_home_page_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create uw_catering_home_page content'.
  $permissions['create uw_catering_home_page content'] = array(
    'name' => 'create uw_catering_home_page content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any uw_catering_home_page content'.
  $permissions['delete any uw_catering_home_page content'] = array(
    'name' => 'delete any uw_catering_home_page content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own uw_catering_home_page content'.
  $permissions['delete own uw_catering_home_page content'] = array(
    'name' => 'delete own uw_catering_home_page content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any uw_catering_home_page content'.
  $permissions['edit any uw_catering_home_page content'] = array(
    'name' => 'edit any uw_catering_home_page content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own uw_catering_home_page content'.
  $permissions['edit own uw_catering_home_page content'] = array(
    'name' => 'edit own uw_catering_home_page content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  return $permissions;
}
