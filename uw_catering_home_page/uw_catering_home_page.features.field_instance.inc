<?php

/**
 * @file
 * uw_catering_home_page.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function uw_catering_home_page_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'node-uw_catering_home_page-body'.
  $field_instances['node-uw_catering_home_page-body'] = array(
    'bundle' => 'uw_catering_home_page',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'banner' => array(
        'label' => 'hidden',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 1,
      ),
      'default' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(
          'field_formatter_label' => '',
        ),
        'type' => 'text_default',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(
          'field_formatter_label' => '',
          'trim_length' => 600,
        ),
        'type' => 'text_summary_or_trimmed',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'body',
    'label' => 'Body',
    'required' => FALSE,
    'settings' => array(
      'display_summary' => TRUE,
      'entity_translation_sync' => FALSE,
      'text_processing' => 1,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'module' => 'text',
      'settings' => array(
        'rows' => 20,
        'summary_rows' => 5,
      ),
      'type' => 'text_textarea_with_summary',
      'weight' => 2,
    ),
  );

  // Exported field_instance: 'node-uw_catering_home_page-field_home_banners'.
  $field_instances['node-uw_catering_home_page-field_home_banners'] = array(
    'bundle' => 'uw_catering_home_page',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'banner' => array(
        'label' => 'hidden',
        'module' => 'paragraphs',
        'settings' => array(
          'field_formatter_label' => '',
          'view_mode' => 'full',
        ),
        'type' => 'paragraphs_view',
        'weight' => 0,
      ),
      'default' => array(
        'label' => 'hidden',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 2,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'fences_wrapper' => 'ul',
    'field_name' => 'field_home_banners',
    'label' => 'Banners',
    'required' => 0,
    'settings' => array(
      'add_mode' => 'select',
      'allowed_bundles' => array(
        'banner_with_link' => 'banner_with_link',
        'content_section' => -1,
        'gallery' => -1,
        'menu_item' => -1,
        'menu_item_price' => -1,
        'menu_option' => -1,
        'menu_section' => -1,
        'text' => -1,
        'text_with_image' => -1,
        'views_embed' => -1,
      ),
      'bundle_weights' => array(
        'banner_with_link' => -4,
        'content_section' => 6,
        'gallery' => 7,
        'menu_item' => 8,
        'menu_item_price' => 9,
        'menu_option' => 10,
        'menu_section' => 11,
        'text' => 12,
        'text_with_image' => 3,
        'views_embed' => 14,
      ),
      'default_edit_mode' => 'open',
      'entity_translation_sync' => FALSE,
      'title' => 'Banner Image',
      'title_multiple' => 'Banner Images',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'paragraphs',
      'settings' => array(
        'label_help_description' => '',
      ),
      'type' => 'paragraphs_embed',
      'weight' => 1,
    ),
  );

  // Exported field_instance: 'node-uw_catering_home_page-field_paragraphs'.
  $field_instances['node-uw_catering_home_page-field_paragraphs'] = array(
    'bundle' => 'uw_catering_home_page',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'banner' => array(
        'label' => 'hidden',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 2,
      ),
      'default' => array(
        'label' => 'hidden',
        'module' => 'paragraphs',
        'settings' => array(
          'field_formatter_label' => '',
          'view_mode' => 'full',
        ),
        'type' => 'paragraphs_view',
        'weight' => 1,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'fences_wrapper' => 'div',
    'field_name' => 'field_paragraphs',
    'label' => 'Content',
    'required' => 0,
    'settings' => array(
      'add_mode' => 'select',
      'allowed_bundles' => array(
        'banner_with_link' => -1,
        'gallery' => -1,
        'menu_item' => -1,
        'menu_item_price' => -1,
        'menu_section' => -1,
        'text' => -1,
        'text_with_image' => 'text_with_image',
        'views_embed' => -1,
      ),
      'bundle_weights' => array(
        'banner_with_link' => 5,
        'gallery' => 6,
        'menu_item' => 7,
        'menu_item_price' => 8,
        'menu_section' => 9,
        'text' => 10,
        'text_with_image' => 3,
        'views_embed' => 12,
      ),
      'default_edit_mode' => 'open',
      'entity_translation_sync' => FALSE,
      'title' => 'Section',
      'title_multiple' => 'Sections',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'paragraphs',
      'settings' => array(
        'label_help_description' => '',
      ),
      'type' => 'paragraphs_embed',
      'weight' => 3,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Banners');
  t('Body');
  t('Content');

  return $field_instances;
}
