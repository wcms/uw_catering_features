<?php

/**
 * @file
 * uw_catering_home_page.default_breakpoint_group.inc
 */

/**
 * Implements hook_default_breakpoint_group().
 */
function uw_catering_home_page_default_breakpoint_group() {
  $export = array();

  $breakpoint_group = new stdClass();
  $breakpoint_group->disabled = FALSE; /* Edit this to true to make a default breakpoint_group disabled initially */
  $breakpoint_group->api_version = 1;
  $breakpoint_group->machine_name = 'uw_catering_theme';
  $breakpoint_group->name = 'uWaterloo Catering Services Custom Theme';
  $breakpoint_group->breakpoints = array(
    0 => 'breakpoints.theme.uw_catering_theme.mobile',
    1 => 'breakpoints.theme.uw_catering_theme.narrow',
    2 => 'breakpoints.theme.uw_catering_theme.wide',
  );
  $breakpoint_group->type = 'theme';
  $breakpoint_group->overridden = 0;
  $export['uw_catering_theme'] = $breakpoint_group;

  return $export;
}
