<?php

/**
 * @file
 * uw_catering_people.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function uw_catering_people_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function uw_catering_people_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_field_default_field_instances_alter().
 */
function uw_catering_people_field_default_field_instances_alter(&$data) {
  if (isset($data['node-contact-field_contact_image'])) {
    $data['node-contact-field_contact_image']['display']['banner'] = array(
      'label' => 'above',
      'settings' => array(),
      'type' => 'hidden',
      'weight' => 11,
    ); /* WAS: '' */
    $data['node-contact-field_contact_image']['display']['default']['settings']['field_formatter_label'] = ''; /* WAS: '' */
    $data['node-contact-field_contact_image']['display']['default']['settings']['image_style'] = 'catering_gallery_thumbnail'; /* WAS: 'thumbnail' */
    $data['node-contact-field_contact_image']['display']['teaser']['label'] = 'hidden'; /* WAS: 'above' */
    $data['node-contact-field_contact_image']['display']['teaser']['module'] = 'image'; /* WAS: '' */
    $data['node-contact-field_contact_image']['display']['teaser']['settings']['field_formatter_label'] = ''; /* WAS: '' */
    $data['node-contact-field_contact_image']['display']['teaser']['settings']['image_link'] = ''; /* WAS: '' */
    $data['node-contact-field_contact_image']['display']['teaser']['settings']['image_style'] = 'catering_gallery_thumbnail'; /* WAS: '' */
    $data['node-contact-field_contact_image']['display']['teaser']['type'] = 'image'; /* WAS: 'hidden' */
    $data['node-contact-field_contact_image']['fences_wrapper'] = 'div'; /* WAS: '' */
    $data['node-contact-field_contact_image']['settings']['focal_point_preview'] = 0; /* WAS: '' */
    $data['node-contact-field_contact_image']['settings']['focal_point_styles'] = array(
      'catering_banner__med' => 0,
      'catering_banner__sm' => 0,
      'catering_banner__wide' => 0,
      'catering_gallery_thumbnail' => 'catering_gallery_thumbnail',
      'catering_short_banner__med' => 0,
      'catering_short_banner__sm' => 0,
      'catering_short_banner__wide' => 0,
    ); /* WAS: '' */
    $data['node-contact-field_contact_image']['widget']['settings']['insert_styles']['colorbox__catering_banner__med'] = 0; /* WAS: '' */
    $data['node-contact-field_contact_image']['widget']['settings']['insert_styles']['colorbox__catering_banner__sm'] = 0; /* WAS: '' */
    $data['node-contact-field_contact_image']['widget']['settings']['insert_styles']['colorbox__catering_banner__wide'] = 0; /* WAS: '' */
    $data['node-contact-field_contact_image']['widget']['settings']['insert_styles']['colorbox__catering_gallery_thumbnail'] = 0; /* WAS: '' */
    $data['node-contact-field_contact_image']['widget']['settings']['insert_styles']['colorbox__catering_homepage_banner_lg'] = 0; /* WAS: '' */
    $data['node-contact-field_contact_image']['widget']['settings']['insert_styles']['colorbox__catering_homepage_banner_sm'] = 0; /* WAS: '' */
    $data['node-contact-field_contact_image']['widget']['settings']['insert_styles']['colorbox__catering_short_banner__med'] = 0; /* WAS: '' */
    $data['node-contact-field_contact_image']['widget']['settings']['insert_styles']['colorbox__catering_short_banner__sm'] = 0; /* WAS: '' */
    $data['node-contact-field_contact_image']['widget']['settings']['insert_styles']['colorbox__catering_short_banner__wide'] = 0; /* WAS: '' */
    $data['node-contact-field_contact_image']['widget']['settings']['insert_styles']['colorbox__gallery_lightbox'] = 0; /* WAS: '' */
    $data['node-contact-field_contact_image']['widget']['settings']['insert_styles']['colorbox__linkit_thumb'] = 0; /* WAS: '' */
    $data['node-contact-field_contact_image']['widget']['settings']['insert_styles']['image_catering_banner__med'] = 0; /* WAS: '' */
    $data['node-contact-field_contact_image']['widget']['settings']['insert_styles']['image_catering_banner__sm'] = 0; /* WAS: '' */
    $data['node-contact-field_contact_image']['widget']['settings']['insert_styles']['image_catering_banner__wide'] = 0; /* WAS: '' */
    $data['node-contact-field_contact_image']['widget']['settings']['insert_styles']['image_catering_gallery_thumbnail'] = 0; /* WAS: '' */
    $data['node-contact-field_contact_image']['widget']['settings']['insert_styles']['image_catering_homepage_banner_lg'] = 0; /* WAS: '' */
    $data['node-contact-field_contact_image']['widget']['settings']['insert_styles']['image_catering_homepage_banner_sm'] = 0; /* WAS: '' */
    $data['node-contact-field_contact_image']['widget']['settings']['insert_styles']['image_catering_short_banner__med'] = 0; /* WAS: '' */
    $data['node-contact-field_contact_image']['widget']['settings']['insert_styles']['image_catering_short_banner__sm'] = 0; /* WAS: '' */
    $data['node-contact-field_contact_image']['widget']['settings']['insert_styles']['image_catering_short_banner__wide'] = 0; /* WAS: '' */
    $data['node-contact-field_contact_image']['widget']['settings']['insert_styles']['image_gallery_lightbox'] = 0; /* WAS: '' */
    $data['node-contact-field_contact_image']['widget']['settings']['insert_styles']['image_linkit_thumb'] = 0; /* WAS: '' */
    $data['node-contact-field_contact_image']['widget']['settings']['preview_image_style'] = 'focal_point_preview'; /* WAS: 'thumbnail' */
    unset($data['node-contact-field_contact_image']['settings']['image_field_caption']);
    unset($data['node-contact-field_contact_image']['settings']['image_field_caption_wrapper']);
    unset($data['node-contact-field_contact_image']['widget']['settings']['insert_styles']['colorbox__autocompletion_style']);
    unset($data['node-contact-field_contact_image']['widget']['settings']['insert_styles']['colorbox__banner-750w']);
    unset($data['node-contact-field_contact_image']['widget']['settings']['insert_styles']['colorbox__banner-wide']);
    unset($data['node-contact-field_contact_image']['widget']['settings']['insert_styles']['colorbox__field-slideshow-slide']);
    unset($data['node-contact-field_contact_image']['widget']['settings']['insert_styles']['colorbox__image_gallery_squares']);
    unset($data['node-contact-field_contact_image']['widget']['settings']['insert_styles']['colorbox__person-profile-list']);
    unset($data['node-contact-field_contact_image']['widget']['settings']['insert_styles']['colorbox__profile-photo-block']);
    unset($data['node-contact-field_contact_image']['widget']['settings']['insert_styles']['colorbox__uw_service_icon']);
    unset($data['node-contact-field_contact_image']['widget']['settings']['insert_styles']['image_autocompletion_style']);
    unset($data['node-contact-field_contact_image']['widget']['settings']['insert_styles']['image_banner-750w']);
    unset($data['node-contact-field_contact_image']['widget']['settings']['insert_styles']['image_banner-wide']);
    unset($data['node-contact-field_contact_image']['widget']['settings']['insert_styles']['image_field-slideshow-slide']);
    unset($data['node-contact-field_contact_image']['widget']['settings']['insert_styles']['image_image_gallery_squares']);
    unset($data['node-contact-field_contact_image']['widget']['settings']['insert_styles']['image_person-profile-list']);
    unset($data['node-contact-field_contact_image']['widget']['settings']['insert_styles']['image_profile-photo-block']);
    unset($data['node-contact-field_contact_image']['widget']['settings']['insert_styles']['image_uw_service_icon']);
  }
  if (isset($data['node-contact-field_contact_title'])) {
    $data['node-contact-field_contact_title']['display']['banner'] = array(
      'label' => 'above',
      'settings' => array(),
      'type' => 'hidden',
      'weight' => 1,
    ); /* WAS: '' */
    $data['node-contact-field_contact_title']['display']['default']['settings']['field_formatter_label'] = ''; /* WAS: '' */
    $data['node-contact-field_contact_title']['display']['teaser']['label'] = 'hidden'; /* WAS: 'above' */
    $data['node-contact-field_contact_title']['display']['teaser']['module'] = 'text'; /* WAS: '' */
    $data['node-contact-field_contact_title']['display']['teaser']['settings']['field_formatter_label'] = ''; /* WAS: '' */
    $data['node-contact-field_contact_title']['display']['teaser']['type'] = 'text_default'; /* WAS: 'hidden' */
    $data['node-contact-field_contact_title']['display']['teaser']['weight'] = 1; /* WAS: 0 */
    $data['node-contact-field_contact_title']['fences_wrapper'] = 'p'; /* WAS: '' */
    $data['node-contact-field_contact_title']['settings']['better_formats'] = array(
      'allowed_formats' => array(
        'full_html' => 'full_html',
        'plain_text' => 'plain_text',
        'single_page_remote_events' => 'single_page_remote_events',
        'uw_tf_basic' => 'uw_tf_basic',
        'uw_tf_comment' => 'uw_tf_comment',
        'uw_tf_contact' => 'uw_tf_contact',
        'uw_tf_standard' => 'uw_tf_standard',
        'uw_tf_standard_sidebar' => 'uw_tf_standard_sidebar',
        'uw_tf_standard_site_footer' => 'uw_tf_standard_site_footer',
        'uw_tf_standard_wide' => 'uw_tf_standard_wide',
      ),
      'allowed_formats_toggle' => 0,
      'default_order_toggle' => 0,
      'default_order_wrapper' => array(
        'formats' => array(
          'full_html' => array(
            'weight' => -6,
          ),
          'plain_text' => array(
            'weight' => 10,
          ),
          'single_page_remote_events' => array(
            'weight' => 0,
          ),
          'uw_tf_basic' => array(
            'weight' => 0,
          ),
          'uw_tf_comment' => array(
            'weight' => 0,
          ),
          'uw_tf_contact' => array(
            'weight' => -5,
          ),
          'uw_tf_standard' => array(
            'weight' => -10,
          ),
          'uw_tf_standard_sidebar' => array(
            'weight' => -8,
          ),
          'uw_tf_standard_site_footer' => array(
            'weight' => -7,
          ),
          'uw_tf_standard_wide' => array(
            'weight' => -9,
          ),
        ),
      ),
    ); /* WAS: '' */
    $data['node-contact-field_contact_title']['settings']['linkit'] = array(
      'button_text' => 'Search',
      'enable' => 0,
      'profile' => '',
    ); /* WAS: '' */
    $data['node-contact-field_contact_title']['widget']['settings']['label_help_description'] = ''; /* WAS: '' */
    $data['node-contact-field_contact_title']['widget']['weight'] = 5; /* WAS: 4 */
    unset($data['node-contact-field_contact_title']['settings']['exclude_cv']);
  }
}
