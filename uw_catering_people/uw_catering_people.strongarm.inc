<?php

/**
 * @file
 * uw_catering_people.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function uw_catering_people_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'rh_node_action_contact';
  $strongarm->value = '3';
  $export['rh_node_action_contact'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'rh_node_override_contact';
  $strongarm->value = 1;
  $export['rh_node_override_contact'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'rh_node_redirect_contact';
  $strongarm->value = '/about/people';
  $export['rh_node_redirect_contact'] = $strongarm;

  return $export;
}
