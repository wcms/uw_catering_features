<?php

/**
 * @file
 * uw_catering_forms.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function uw_catering_forms_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'node-uw_catering_form-body'.
  $field_instances['node-uw_catering_form-body'] = array(
    'bundle' => 'uw_catering_form',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'banner' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'default' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(
          'field_formatter_label' => '',
        ),
        'type' => 'text_default',
        'weight' => 0,
      ),
      'embedded' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(
          'field_formatter_label' => '',
          'trim_length' => 600,
        ),
        'type' => 'text_summary_or_trimmed',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'body',
    'label' => 'Body',
    'required' => FALSE,
    'settings' => array(
      'display_summary' => TRUE,
      'entity_translation_sync' => FALSE,
      'text_processing' => 1,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'module' => 'text',
      'settings' => array(
        'rows' => 20,
        'summary_rows' => 5,
      ),
      'type' => 'text_textarea_with_summary',
      'weight' => 41,
    ),
  );

  // Exported field_instance: 'node-uw_catering_form-field_banner_image'.
  $field_instances['node-uw_catering_form-field_banner_image'] = array(
    'bundle' => 'uw_catering_form',
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'banner' => array(
        'label' => 'hidden',
        'module' => 'picture',
        'settings' => array(
          'colorbox_settings' => array(
            'colorbox_caption' => 'auto',
            'colorbox_caption_custom' => '',
            'colorbox_gallery' => 'post',
            'colorbox_gallery_custom' => '',
            'colorbox_group' => '',
            'colorbox_multivalue_index' => NULL,
          ),
          'fallback_image_style' => 'focal_point_preview',
          'field_formatter_label' => '',
          'image_link' => '',
          'image_styles' => array(
            '_empty image_' => 0,
            '_original image_' => 0,
            'body-500px-wide' => 0,
            'catering_banner__med' => 'catering_banner__med',
            'catering_banner__sm' => 'catering_banner__sm',
            'catering_banner__wide' => 'catering_banner__wide',
            'catering_gallery_thumbnail' => 0,
            'catering_homepage_banner_lg' => 0,
            'catering_homepage_banner_sm' => 0,
            'focal_point_preview' => 0,
            'gallery_lightbox' => 0,
            'large' => 0,
            'linkit_thumb' => 0,
            'medium' => 0,
            'sidebar-220px-wide' => 0,
            'thumbnail' => 0,
            'wide-body-750px-wide' => 0,
          ),
          'lazyload' => '',
          'lazyload_aspect_ratio' => '',
          'sizes' => '100vw',
        ),
        'type' => 'picture_sizes_formatter',
        'weight' => 0,
      ),
      'default' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 1,
      ),
      'embedded' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'fences_wrapper' => 'figure',
    'field_name' => 'field_banner_image',
    'label' => 'Banner image',
    'required' => 0,
    'settings' => array(
      'alt_field' => 1,
      'alt_field_required' => 0,
      'default_image' => 0,
      'entity_translation_sync' => FALSE,
      'file_directory' => 'banners',
      'file_extensions' => 'png gif jpg jpeg',
      'focal_point' => 0,
      'focal_point_preview' => 0,
      'focal_point_styles' => array(
        'catering_banner__med' => 0,
        'catering_banner__sm' => 0,
        'catering_banner__wide' => 0,
        'catering_gallery_thumbnail' => 0,
      ),
      'max_filesize' => '',
      'max_resolution' => '',
      'min_resolution' => '',
      'title_field' => 0,
      'title_field_required' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'multiupload_imagefield_widget',
      'settings' => array(
        'filefield_sources' => array(
          'filefield_sources' => array(
            'assetbank' => 'assetbank',
            'attach' => 0,
            'clipboard' => 0,
            'imce' => 'imce',
            'reference' => 'reference',
            'remote' => 'remote',
            'upload' => 'upload',
          ),
          'source_attach' => array(
            'absolute' => 0,
            'attach_mode' => 'move',
            'path' => 'file_attach',
          ),
          'source_imce' => array(
            'imce_mode' => 0,
          ),
          'source_reference' => array(
            'autocomplete' => 0,
            'search_all_fields' => 0,
          ),
        ),
        'insert' => 0,
        'insert_absolute' => 0,
        'insert_class' => '',
        'insert_default' => 'auto',
        'insert_styles' => array(
          'auto' => 'auto',
          'colorbox__body-500px-wide' => 0,
          'colorbox__catering_banner__med' => 0,
          'colorbox__catering_banner__sm' => 0,
          'colorbox__catering_banner__wide' => 0,
          'colorbox__catering_gallery_thumbnail' => 0,
          'colorbox__catering_homepage_banner_lg' => 0,
          'colorbox__catering_homepage_banner_sm' => 0,
          'colorbox__focal_point_preview' => 0,
          'colorbox__gallery_lightbox' => 0,
          'colorbox__large' => 0,
          'colorbox__linkit_thumb' => 0,
          'colorbox__medium' => 0,
          'colorbox__sidebar-220px-wide' => 0,
          'colorbox__thumbnail' => 0,
          'colorbox__wide-body-750px-wide' => 0,
          'icon_link' => 0,
          'image' => 0,
          'image_body-500px-wide' => 0,
          'image_catering_banner__med' => 0,
          'image_catering_banner__sm' => 0,
          'image_catering_banner__wide' => 0,
          'image_catering_gallery_thumbnail' => 0,
          'image_catering_homepage_banner_lg' => 0,
          'image_catering_homepage_banner_sm' => 0,
          'image_focal_point_preview' => 0,
          'image_gallery_lightbox' => 0,
          'image_large' => 0,
          'image_linkit_thumb' => 0,
          'image_medium' => 0,
          'image_sidebar-220px-wide' => 0,
          'image_thumbnail' => 0,
          'image_wide-body-750px-wide' => 0,
          'link' => 0,
        ),
        'insert_width' => '',
        'label_help_description' => '',
        'preview_image_style' => 'thumbnail',
        'progress_indicator' => 'throbber',
      ),
      'type' => 'image_miw',
      'weight' => 1,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Banner image');
  t('Body');

  return $field_instances;
}
