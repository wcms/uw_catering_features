<?php

/**
 * @file
 * uw_catering_forms.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function uw_catering_forms_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'access all webform results'.
  $permissions['access all webform results'] = array(
    'name' => 'access all webform results',
    'roles' => array(
      'administrator' => 'administrator',
      'form editor' => 'form editor',
      'form results access' => 'form results access',
      'site manager' => 'site manager',
    ),
    'module' => 'webform',
  );

  // Exported permission: 'access own webform results'.
  $permissions['access own webform results'] = array(
    'name' => 'access own webform results',
    'roles' => array(
      'administrator' => 'administrator',
      'form results access' => 'form results access',
    ),
    'module' => 'webform',
  );

  // Exported permission: 'access own webform submissions'.
  $permissions['access own webform submissions'] = array(
    'name' => 'access own webform submissions',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'webform',
  );

  // Exported permission: 'add users to webform results access'.
  $permissions['add users to webform results access'] = array(
    'name' => 'add users to webform results access',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'webform_access_granular',
  );

  // Exported permission: 'bypass granular form results access'.
  $permissions['bypass granular form results access'] = array(
    'name' => 'bypass granular form results access',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'webform_access_granular',
  );

  // Exported permission: 'create uw_catering_form content'.
  $permissions['create uw_catering_form content'] = array(
    'name' => 'create uw_catering_form content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'form editor' => 'form editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete all webform submissions'.
  $permissions['delete all webform submissions'] = array(
    'name' => 'delete all webform submissions',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'webform',
  );

  // Exported permission: 'delete any uw_catering_form content'.
  $permissions['delete any uw_catering_form content'] = array(
    'name' => 'delete any uw_catering_form content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own uw_catering_form content'.
  $permissions['delete own uw_catering_form content'] = array(
    'name' => 'delete own uw_catering_form content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own webform submissions'.
  $permissions['delete own webform submissions'] = array(
    'name' => 'delete own webform submissions',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'webform',
  );

  // Exported permission: 'edit all webform submissions'.
  $permissions['edit all webform submissions'] = array(
    'name' => 'edit all webform submissions',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'webform',
  );

  // Exported permission: 'edit any uw_catering_form content'.
  $permissions['edit any uw_catering_form content'] = array(
    'name' => 'edit any uw_catering_form content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'form editor' => 'form editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own uw_catering_form content'.
  $permissions['edit own uw_catering_form content'] = array(
    'name' => 'edit own uw_catering_form content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'form editor' => 'form editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own webform submissions'.
  $permissions['edit own webform submissions'] = array(
    'name' => 'edit own webform submissions',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'webform',
  );

  // Exported permission: 'edit webform components'.
  $permissions['edit webform components'] = array(
    'name' => 'edit webform components',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'form editor' => 'form editor',
      'site manager' => 'site manager',
    ),
    'module' => 'webform',
  );

  return $permissions;
}
