<?php

/**
 * @file
 * uw_catering_forms.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function uw_catering_forms_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function uw_catering_forms_node_info() {
  $items = array(
    'uw_catering_form' => array(
      'name' => t('Form'),
      'base' => 'node_content',
      'description' => t('A custom form, with different types of input fields, confirmation messages, and email forwarding.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
